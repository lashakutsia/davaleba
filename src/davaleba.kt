fun main() {
    val x = Fraction( 7.0,  9.0)
    println(x)
    val y = Fraction( 10.0,  18.0)
    println(x)
    println(y)
    println(x.add(y))
    println(x.multiply(y))
    println(x.minus(y))
    println(x.divide(y))
}

class Fraction(val numerator : Double, val denominator: Double) {
    override fun toString(): String {
        return "$numerator / $denominator"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return (numerator * other.numerator == other.denominator * denominator)
        }
        return false
    }

    fun add(other: Fraction): Fraction {
        val newdenominator = denominator * other.denominator
        val newnumerator1 = newdenominator / denominator * numerator
        val newnumerator2 = newdenominator / other.denominator * other.numerator
        return Fraction(newnumerator1 + newnumerator2, newdenominator)

    }


    fun multiply(other: Fraction): Fraction {
        val newdenominator = denominator * other.denominator
        val newnumerator = numerator * other.numerator
        return Fraction(newnumerator, newdenominator)
    }

    fun minus(other: Fraction): Fraction {
        return add(Fraction(-1 * other.numerator, other.denominator))
    }

    fun divide(other: Fraction): Fraction {
        return multiply(Fraction(other.denominator, other.numerator))
    }
}




